const inventory = require('./inventory');

const ageSort = inventory.sort((element1, element2) => {
    if(element1["age"] < element2["age"]){
        return 1;
    }
    if(element1["age"] > element2["age"]){
        return -1;
    }else{
        return 0;
    }
    return ageSort;
})

console.log(ageSort);