const inventory = require('./inventory');

function dateFormat(inventory){
    let dates = inventory.map((element) => {
        let value = element["registered"].split("T");
        element['registered'] =  value[0].split("-").reverse().join("/");
        return element;
        
    })
    return dates;
}
console.log(dateFormat(inventory));