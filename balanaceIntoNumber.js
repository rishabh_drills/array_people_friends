const inventory = require('./inventory');

function balanceToNumber(inventory){
    let convertedBalance = inventory.map((element) => {
        let value = element["balance"].replace("$", "").replace(",", "");
        // console.log(value);
        // value = element["balance"].replace(",", "");
        // console.log(value);
        if(isNaN(value)){
            element["newBalance"] = 0;
        }else{
            element["newBalance"] = value;
        }
        return element;
        // console.log(value);
    });
    return convertedBalance;
}

let result = balanceToNumber(inventory);
console.log(result);